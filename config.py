from os import environ, path
from dotenv import load_dotenv

basedir = path.abspath(path.dirname(__file__))
load_dotenv(path.join(basedir, '.env'))

class Config:
    SECRET_KEY = environ.get('SECRET_KEY')
    SECURITY_PASSWORD_SALT = environ.get('SECURITY_PASSWORD_SALT')
    #SESSION_COOKIE_NAME = environ.get('SESSION_COOKIE_NAME')


class DevelopmentConfig(Config):
    FLASK_ENV = 'development'
    DEBUG=True
    SQLALCHEMY_DATABASE_URI=environ.get('DEV_DATABASE_URI')
    SQLALCHEMY_ECHO = False
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    # mail settings
    MAIL_SERVER=environ.get('MAIL_SERVER')
    MAIL_PORT=environ.get('MAIL_PORT')
    MAIL_USE_TLS= False
    MAIL_USE_SSL= True
    MAIL_USERNAME=environ.get('MAIL_USERNAME')
    MAIL_PASSWORD=environ.get('MAIL_PASSWORD')
    MAIL_DEFAULT_SENDER=environ.get('MAIL_DEFAULT_SENDER')

class ProductionConfig(Config):
    FLASK_ENV = 'production'
    DEBUG=True
    SQLALCHEMY_DATABASE_URI=environ.get('PROD_DATABASE_URI')
    SQLALCHEMY_ECHO = False
    SQLALCHEMY_TRACK_MODIFICATIONS = False
