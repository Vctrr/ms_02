from src.api.model.userDetail_model import UserDetail
from app import db,ma

class UserDetailSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = UserDetail
        load_instance=True
        sqla_session = db.session
    id_usdetail = ma.auto_field() 
    firstname = ma.auto_field()
    lastname = ma.auto_field()
    created_at = ma.auto_field()
    updated_at = ma.auto_field()
    