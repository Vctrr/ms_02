from marshmallow import fields
from src.api.model.user_model import Users
from src.api.schema.userDetail_schema import UserDetail
from app import db,ma

class UsersSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Users
        load_instance=True
        sqla_session = db.session
    id_user = ma.auto_field() 
    email = ma.auto_field() 
    password = ma.auto_field() 
    is_active = ma.auto_field() 
    is_deleted = ma.auto_field() 
    confirmed = ma.auto_field() 
    confirmed_on = ma.auto_field() 
    created_at = ma.auto_field() 
    updated_at = ma.auto_field()
    users = fields.Nested('UserDetail')