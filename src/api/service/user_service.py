from flask import Blueprint, request, jsonify, make_response, url_for
from sqlalchemy.sql.expression import null
from src.api.schema.user_schema import UsersSchema
from src.api.schema.userDetail_schema import UserDetail
from src.api.model.user_model import Users
from flask_jwt_extended import create_access_token
from flask_jwt_extended import jwt_required, get_jwt_identity
from src.email_token import generate_confirmation_token, confirm_token
from src.email import send_email
from app import db,ma
from datetime import datetime

user_blueprint = Blueprint('user', __name__)

#Sin Acceso de Token
@user_blueprint.route("/user/hello")
def hello_no_token():
    return "Hello NO TOKEN!"

@user_blueprint.route("/user")
@jwt_required()
def hello_token():
    return "Hello USER!"


@user_blueprint.route("/user/token", methods = ['POST'])
def create_token():
    email = request.json.get("email", None)
    password = request.json.get("password", None)
    # Consulta la base de datos por el nombre de usuario y la contraseña
    user = Users.query.filter_by(email=email).first()
    print (user)
    
    if not user or not user.verify_password(password):
          # el usuario no se encontró en la base de datos
        return make_response(jsonify({"msg": "Bad username or password"}), 401)
    
    if not user.is_active and user.confirmed:
        return make_response(jsonify({"msg": "Not confirmed"}), 401)
    # crea un nuevo token con el id de usuario dentro
    access_token = create_access_token(identity=user.id_user)
    return make_response(jsonify({ "token": access_token, "user_id": user.id_user , "username": user.email}))

@user_blueprint.route('/user/list', methods = ['GET'])
@jwt_required()
def list_users():
    get_users = Users.query.all()
    user_schema = UsersSchema(many=True)
    lista = user_schema.dump(get_users)
    return make_response(jsonify({"users": lista}))

@user_blueprint.route('/user/<id>', methods = ['GET'])
@jwt_required()
def get_user_by_id(id):
    user = Users.query.get(id)
    user_schema = UsersSchema()
    user_by_one = user_schema.dump(user)
    return make_response(jsonify({"User": user_by_one}))

@user_blueprint.route('/user/create', methods = ['POST'])
def create_user():
    data = request.get_json()
    user_schema = UsersSchema()
    user = user_schema.load(data)
    user.encrypt_password()
    user.save()
    result = user_schema.dump(user)
    token = generate_confirmation_token(user.email)
    body = url_for('user.confirmed_user', token=token, _external=True)
    print(body)
    subject = "Please confirm your email"
    send_email(user.email, subject, body)
    return make_response(jsonify({"user": result}),200)

@user_blueprint.route('/user/confirmed/<token>')
def confirmed_user(token):
    try:        
        email = confirm_token(token)
    except:
        msg = 'The confirmation link is invalid or has expired.'
    get_user = Users.query.filter_by(email=email).first_or_404()
    if get_user.confirmed:
        msg = 'Account already confirmed. Please login.'
    else:
        get_user.is_active = True
        get_user.confirmed = True
        get_user.confirmed_on = "2021-10-22T12:03:37"
        user_schema = UsersSchema(only=['is_active','confirmed','confirmed_on'])
        resp = user_schema.dump(get_user.update())
        msg = 'You have confirmed your account. Thanks!'
    return make_response(jsonify({"msg": msg}))


@user_blueprint.route('/customer/create', methods = ['POST'])
def create_customer():
    data = request.get_json()
    cust_schema = UserDetail()
    customer = cust_schema.load(data)
    customer.users.encrypt_password()
    #customer.save()
    result = cust_schema.dump(customer)
    print(customer.users.email)
    token = generate_confirmation_token(customer.users.email)

    body = ('http://127.0.0.1:5000/customer/confirmed/'+token)    
    subject = "Please confirm your email"
    send_email(customer.users.email, subject, body)

    #email = confirm_token(token)
    return make_response(jsonify({"token": token,"customer": result}),200)
    #return make_response(jsonify({"token_confirmated": email,"token": token,"customer": result}),200)

'''
@user_blueprint.route('/user/update/<id>', methods = ['PUT'])
def update_inicio_by_id(id):
    data = request.get_json()
    get_inicio = Users.query.get(id)
    if data.get('id'):
        get_inicio.id = data['id']
    if data.get('descripcion'):
        get_inicio.descripcion = data['descripcion']
    home_schema = UsersSchema(only=['id', 'descripcion'])
    inicio = home_schema.dump(get_inicio.update())
    return make_response(jsonify({"inicio": inicio}))
'''