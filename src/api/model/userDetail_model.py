# import db object from flask app
from app import db
from src.db import BaseModelMixin
from sqlalchemy.sql import func

# database tables
class UserDetail(db.Model,BaseModelMixin):
     __tablename__ = "user_detail"
     id_usdetail = db.Column(db.Integer, primary_key =True, autoincrement=True)
     firstname = db.Column(db.String(25))
     lastname = db.Column(db.String(50))
     telephone = db.Column(db.Integer)
     birthday = db.Column(db.DateTime(timezone=True), default=func.now())
     created_at = db.Column(db.DateTime(timezone=True), default=func.now())
     updated_at = db.Column(db.DateTime(timezone=True), default=func.now())
     user_id = db.Column(db.Integer, db.ForeignKey('users.id_user'))

     def __init__(self, firstname, lastname , telephone, birthday, created_at ,updated_at):
          self.firstname = firstname
          self.lastname = lastname
          self.telephone = telephone
          self.birthday = birthday
          self.created_at = created_at
          self.updated_at = updated_at

     def __repr__(self):
          return f"{self.firstname}"

     def update(self):
          db.session.add(self)
          db.session.commit()
          return self