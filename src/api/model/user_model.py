# import db object from flask app
from app import db
from src.db import BaseModelMixin
from sqlalchemy.sql import func
from werkzeug.security import check_password_hash, generate_password_hash
from src.api.model.userDetail_model import UserDetail

# database tables
class Users(db.Model,BaseModelMixin):
     __tablename__ = "users"
     id_user = db.Column(db.Integer, primary_key =True, autoincrement=True)
     email = db.Column(db.String(100), unique=True)
     password = db.Column(db.String(500))
     is_active = db.Column(db.Boolean, default=1)
     is_deleted = db.Column(db.Boolean, default=0)
     confirmed = db.Column(db.Boolean, nullable=False, default=False)
     confirmed_on = db.Column(db.DateTime, nullable=True)
     created_at = db.Column(db.DateTime(timezone=True), default=func.now())
     updated_at = db.Column(db.DateTime(timezone=True), default=func.now())
     user_detail = db.relationship('UserDetail', backref='Users', uselist=False)

     def __init__(self, email ,password ,is_active, is_deleted, confirmed, confirmed_on, created_at, updated_at):
          self.email = email
          self.password = password
          self.is_active = is_active
          self.is_deleted = is_deleted
          self.confirmed = confirmed
          self.confirmed_on = confirmed_on
          self.created_at = created_at
          self.updated_at = updated_at

     def __repr__(self):
          return f"{self.email}"

     
     def encrypt_password(self):
          self.password = generate_password_hash(self.password)


     def verify_password(self,password):
          return check_password_hash(self.password, password)
     
     def update(self):
          db.session.add(self)
          db.session.commit()
          return self