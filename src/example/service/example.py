from flask import Blueprint, request, jsonify, make_response
from src.example.schema.example_schema import HomeSchema
from src.example.model.example_model import Home
from app import db,ma

example_blueprint = Blueprint('example', __name__)

@example_blueprint.route("/")
def hola():
    return "Hello World!"

@example_blueprint.route('/example/list', methods = ['GET'])
def list():
    get_home_s = Home.query.all()
    home_schema = HomeSchema(many=True)
    lista = home_schema.dump(get_home_s)
    return make_response(jsonify({"home": lista}))

@example_blueprint.route('/example/<id>', methods = ['GET'])
def get_inicio_by_id(id):
    get_inicio = Home.query.get(id)
    home_schema = HomeSchema()
    inicio_one = home_schema.dump(get_inicio)
    return make_response(jsonify({"inicio": inicio_one}))

@example_blueprint.route('/example/register', methods = ['POST'])
def create_inicio():
    data = request.get_json()
    home_schema = HomeSchema()
    inicio = home_schema.load(data)
    inicio.save()
    result = home_schema.dump(inicio)
    return make_response(jsonify({"home": result}),200)

@example_blueprint.route('/example/update/<id>', methods = ['PUT'])
def update_inicio_by_id(id):
    data = request.get_json()
    get_inicio = Home.query.get(id)
    if data.get('id'):
        get_inicio.id = data['id']
    if data.get('descripcion'):
        get_inicio.descripcion = data['descripcion']
    home_schema = HomeSchema(only=['id', 'descripcion'])
    inicio = home_schema.dump(get_inicio.update())
    return make_response(jsonify({"inicio": inicio}))
