from marshmallow import fields
from src.example.model.example_model import Home
from app import db,ma

class HomeSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Home
        load_instance=True
        sqla_session = db.session
    id = ma.auto_field()
    descripcion= ma.auto_field()
