# import db object from flask app
from app import db
from src.db import BaseModelMixin

# database tables
class Home(db.Model,BaseModelMixin):
   id = db.Column(db.Integer, primary_key =True)
   descripcion = db.Column(db.String(255), nullable = False)

   def update(self):
       db.session.add(self)
       db.session.commit()
       return self

   def __init__(self, id, descripcion):
        self.id = id
        self.descripcion = descripcion

   def __repr__(self):
        return f"{self.id}"


#db.create_all()
#   def __init__(self, **kwargs):
#        super(Home, self).__init__(**kwargs)
