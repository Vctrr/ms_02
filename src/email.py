from flask_mail import Message
from app import app, mail


def send_email(to, subject, message):
    msg = Message(
        subject,
        recipients=[to],
        body=message,
        sender=app.config['MAIL_DEFAULT_SENDER']
    )
    mail.send(msg)