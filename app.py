from flask import Flask
import flask
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from flask_jwt_extended import JWTManager
from flask_mail import Mail
#from config import config
app = Flask(__name__)
db = SQLAlchemy()
ma = Marshmallow()
jwt = JWTManager()
mail = Mail()

def create_app():
    
    app.config.from_object('config.DevelopmentConfig')
    #app.config.from_object('config.ProductionConfig')
    db.init_app(app)
    ma.init_app(app)
    jwt.init_app(app)
    mail.init_app(app)

    with app.app_context():
        # Migrations
        from src.api.schema.userDetail_schema import UserDetailSchema
        from src.api.schema.user_schema import UsersSchema
        from src.api.model.userDetail_model import UserDetail
        from src.api.model.user_model import Users
        
        db.create_all()

        # Imports
        #from src.example.service.example import example_blueprint
        from src.api.service.user_service import user_blueprint
        # Register Blueprints
        #app.register_blueprint(example_blueprint)
        app.register_blueprint(user_blueprint)

        return app
